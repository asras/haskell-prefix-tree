import qualified Data.Set as S
import Data.Set (Set)
import qualified Data.Map as M
import Data.Map (Map)
import Data.List (tails)


-- ######################
-- TRIE TYPE DEF
-- ######################
-- A Trie is either the Root or a node.
-- A node has a character and a flag to indicate whether it is a valid end of a string.
-- A node also has a set of children.
data Trie = Root (Set Trie) | Trie Char Bool (Set Trie) deriving (Eq, Ord)

-- The empty Trie
empty :: Trie
empty = Root S.empty 
-- ######################
-- END OF TRIE TYPE DEF
-- ######################



-- ######################
-- UTILITY
-- ######################
-- We make a custom instance for show for Trie because 
-- otherwise it's completely impossible to see what's going on.
instance Show Trie where 
    show = printTrie 0 

-- We indent each level of the trie with 3 more "-" than the previous level
printTrie :: Int -> Trie -> String
printTrie indentLevel (Root children) = replicate (3 * indentLevel) '-' 
            ++ "Root:" 
            ++ S.foldr (\s acc -> acc ++ "\n" ++ printTrie (indentLevel + 1) s) "" children
-- Valid ends of strings are indicated with | . |
printTrie indentLevel (Trie a True children) = replicate (3 * indentLevel) '-' 
            ++ "|" ++ show a ++ "|"
            ++ ":" 
            ++ S.foldr (\s acc -> acc ++ "\n" ++ printTrie (indentLevel + 1) s) "" children
printTrie indentLevel (Trie a False children) = replicate (3 * indentLevel) '-' 
            ++ show a
            ++ ":" 
            ++ S.foldr (\s acc -> acc ++ "\n" ++ printTrie (indentLevel + 1) s) "" children


-- Pipe operator
(|>) x f = f x
-- ######################
-- END OF UTILITY
-- ######################



-- ######################
-- TRIE FUNCTIONS
-- ######################
-- Turn a list of strings into a Trie by inserting each string sequentially.
fromList :: [String] -> Trie
fromList list = foldr (\str trie -> insert str trie) empty list


-- Insert a string into the Trie
insert :: String -> Trie -> Trie
-- Empty string changes nothing
insert [] trie = trie
-- Inserting a non-empty string into the root first checks if any children starts with the same character
-- Then it inserts the string in the matching subtree if any or makes a new subtree
insert (a:rest) (Root children)
    | any (hasValue a) children = Root $ makeNewChildren a rest children
    | otherwise                 = Root (S.insert (makeTrie a rest) children)
-- Inserting a string into a subtree works similar to inserting at the root
insert (a:rest) (Trie x b children) 
    | any (hasValue a) children = Trie x b (makeNewChildren a rest children) 
    | otherwise                 = Trie x b (S.insert (makeTrie a rest) children)

-- Assuming a child has the value a, insert rest under that child
-- and insert that new subtree into the other subtrees 
makeNewChildren a rest children = newChildren
    where
        unchangedChildren = S.filter (not . hasValue a) children
        newChild =
            S.filter (hasValue a) children 
            |> S.toList
            |> head
            |> insert rest
        newChildren = S.insert newChild unchangedChildren


-- Utility function to check for a match on the character of the node
hasValue :: Char -> Trie -> Bool
hasValue c (Trie c' _ _)   = c == c'
hasValue c (Root children) = False


-- Utility to make a Trie from a start character and the rest of the string
makeTrie :: Char -> String -> Trie
makeTrie start []       = Trie start True S.empty
makeTrie start (r:rest) = Trie start False $ S.singleton (makeTrie r rest)


-- Trie "elem" function. Check if the string is an element of the Trie
telem :: String -> Trie -> Bool
telem (h:rest) (Root children) 
    = case child of
        Nothing -> False
        Just c  -> telem (h:rest) c
    where
        child :: Maybe Trie
        child = 
            children
            |> S.filter (hasValue h)
            |> S.toList 
            |> headMaybe
        headMaybe []    = Nothing
        headMaybe (a:_) = Just a
telem (h1:[]  ) (Trie h2 True  _      ) = h1 == h2 
telem (h1:[]  ) (Trie _  False _      ) = False
telem (h1:rest) (Trie h2 _    children) 
                            | h1 == h2  = any (telem rest) children
                            | otherwise = False
telem []         _                      = False
-- ######################
-- END OF TRIE FUNCTIONS
-- ######################



-- ######################
-- CODE FOR TESTING
-- ######################
-- Generate all permutations of a string
-- Probably not the best way to do it
combos :: String -> [String]
combos [a] = [[a]]
combos s = concat [ map (ch :) (combos (remove i s)) | (ch, i) <- zip s [0..]]
    where
        remove :: Int -> String -> String
        remove i ls = (take i ls) ++ (drop (i + 1) ls)

-- Count the number of strings in the Trie
countStrs :: Trie -> Int
countStrs (Root         children) =     S.foldr (\child ps -> ps + countStrs child) 0 children
countStrs (Trie _ False children) =     S.foldr (\child ps -> ps + countStrs child) 0 children
countStrs (Trie _ True  children) = 1 + S.foldr (\child ps -> ps + countStrs child) 0 children


-- Handwritten test cases
tt = fromList ["abc", "bca", "cab"]
tests = [
          telem "abc" tt,
          telem "bca" tt,
          telem "cab" tt,
    not $ telem "cba" tt
    ]

-- ######################
-- END OF CODE FOR TESTING
-- ######################


main = do
    let source = "abcdefgh"
    let t          = fromList $ concat $ map combos $ tails source 
    let test_strs  =            concat $ map combos $ tails source 
    let test_strs2 =            concat $ map combos $ tails (source ++ "i")
    print $ all id tests
    print $ length test_strs == countStrs t
    print $ all (flip telem $ t) test_strs
    print $ not $ any (flip telem $ t) test_strs2


