# Prefix trees in haskell

This is a quick implementation of prefix trees in Haskell. It is not particular efficient. The test runs in ~0.4 seconds when compiled with -O3 on my machine.
